package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import jdk.jfr.Percentage;
import model.Patient;

import java.io.IOException;
import java.util.List;

public class ModalWindowViewController extends GridPane {

    List<Patient> patients;

    @FXML
    private ComboBox<Patient> boxPatients;

    public ModalWindowViewController(List<Patient> patients) {
        // set attributes
        this.patients = patients;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ModalWindow.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            // hier wird initialize aufgerufen
            loader.load();
        } catch (IOException error) {
            error.printStackTrace();
        }
    }

    // Important: @FXML must be before initialize to call it automatically in load
    @FXML
    void initialize() {
        Callback<ListView<Patient>, ListCell<Patient>> boxFactory = (item -> new ListCell<>(){
            @Override
            protected void updateItem(Patient item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    // setGraphic(null);
                } else {
                    setText(item.getFullName());
                    // setGraphic( ... any node ...);
                }
            }
        });

        boxPatients.setButtonCell(boxFactory.call(null));
        boxPatients.setCellFactory(boxFactory);
        refresh();
    }

    public void refresh() {
        // Method to fill elements with data, separated to allow external refreshing after initialize
        boxPatients.getItems().addAll(this.patients);
    }

    @FXML
    void onClickAccept(ActionEvent event) {
        Patient selected = boxPatients.getSelectionModel().getSelectedItem();
        if (selected != null) {
            System.out.println(selected.getFullName());
        }
        ((Stage) this.getScene().getWindow()).close();
    }

    @FXML
    void onClickCancel(ActionEvent event) {
        ((Stage) this.getScene().getWindow()).close();
    }

}