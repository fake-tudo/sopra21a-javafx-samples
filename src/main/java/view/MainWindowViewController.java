package view;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Patient;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


public class MainWindowViewController extends GridPane {

    // this should be instantiated in model - only for demonstration purposes
    private List<Patient> patients = Arrays.asList(
            new Patient("Arthur", "Curry"),
            new Patient("Betty", "Kane"),
            new Patient("Bruce", "Wayne"),
            new Patient("Matt", "Murdoc"),
            new Patient("Wade", "Wilson")
    );

    @FXML
    private ListView<Patient> listPatients;

    @FXML
    private TableView<Patient> tablePatients;

    public MainWindowViewController() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MainWindow.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            // hier wird initialize aufgerufen
            loader.load();
        } catch (IOException error) {
            error.printStackTrace();
        }
    }

    @FXML
    void initialize() {
        // List view
        listPatients.setCellFactory(listView -> new ListCell<>(){
            @Override
            protected void updateItem(Patient item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    Button button = new Button();
                    button.setText("Pick");
                    button.setOnAction(event -> {
                        System.out.println("You clicked: " + item.getFullName());
                    });

                    setText(item.getFullName());
                    setGraphic(button);
                }
            }
        });

        // table view
        TableColumn<Patient, String> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        firstNameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getFirstName()));

        TableColumn<Patient, String> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        lastNameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getLastName()));

        tablePatients.getColumns().addAll(Arrays.asList(firstNameCol, lastNameCol));

        refresh();
    }

    public void refresh() {
        // separate method to allow refreshing
        listPatients.getItems().clear();
        listPatients.getItems().addAll(this.patients);

        tablePatients.setItems(FXCollections.observableList(this.patients));
    }

    @FXML
    void onClickOpenDialog(ActionEvent event) {
        // https://code.makery.ch/blog/javafx-dialogs-official/
        TextInputDialog dialog = new TextInputDialog("Vorname");
        dialog.setTitle("Vornamen eingeben");
        dialog.setHeaderText("Gibt einen Vornamen ein:");
        dialog.setContentText("Enter your first name here ...");

        Optional<String> result = dialog.showAndWait();

        // get result of text dialog
        result.ifPresent(name -> System.out.println("Eingegeben: " + name));

//        if (result.isPresent()) {
//            System.out.println("Eingegeben: " + result.get());
//        }
    }

    @FXML
    void onClickOpenWindow(ActionEvent event) {
        // neues Fenster
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Choose patient");
        window.setWidth(200);
        window.setHeight(150);

        // wenn nur eine scene ersetzt werden soll
        Scene scene = new Scene(new ModalWindowViewController(this.patients), 200, 150);

        // wild things will happen
        //ModalWindowViewController child = new ModalWindowViewController(this.patients);
        //this.getChildren().add(child);

        // aktuelles Fenster ersetzen
        //((Stage) this.getScene().getWindow()).setScene(scene);

        // neues Fenster öffnen
        window.setScene(scene);
        window.showAndWait();
    }

}



